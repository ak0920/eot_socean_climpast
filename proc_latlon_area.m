% proc_latlon_area.m
% 
% Calculates the area of each grid cell for computing spatial means etc.
% 

% Find lat boundaries for each gridcell
latbounds = [-90,-88.75:2.5:88.75,90];

% Set ellipsoid poarameters for Earth (I assume this is correct)
earthellipsoid = referenceSphere('earth','km');

% Create blank fields for absolute and fractional areas
areas_abs = zeros(96,73);
areas_frac = zeros(96,73);

% Calculate area for gridcell at each latitude
for i = 1:73
    area_abs = areaquad(latbounds(i),0,latbounds(i+1),3.75,earthellipsoid);
    area_frac = areaquad(latbounds(i),0,latbounds(i+1),3.75);
    
    % Put into output field
    areas_abs(:,i) = area_abs;
    areas_frac(:,i) = area_frac;
end
