To carry out the analysis as was done in Kennedy-Asser et al. 2020 (Climate of the Past), the directories Scripts, HadCM3BL and FOAM should be downloaded and kept in the same directory. 

Additionally, the SO_database_CSV.xlsx file should be downloaded and each sheet should be exported as a .csv with names CP_EOT.csv, CP_Eocene.csv and CP_Oligocene.csv. These files should be put in a directory called Proxies in the same main directory as Scripts etc.

This analysis was carried out with Matlab R2017a.

All analysis carried out by A.T. Kennedy-Asser, University of Bristol, 2019.
