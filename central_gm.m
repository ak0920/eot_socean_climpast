function [output] = central_gm(input)
% [output] = central_gm(input)
% Sets the Greenwich Meridian at the centre of the data for UM output/data
% interpolated onto the UM (HadCM3BL) grid.
% 

if length(input(:,1)) < 200
    
    if length(input(:,1)) < length(input(1,:))
        output = [input(:,49:96),input(:,1:48)];
    else
        output = [input(49:96,:);input(1:48,:)];
    end
    
else
    if length(input(:,1)) < length(input(1,:))
        output = [input(:,361:720),input(:,1:360)];
    else
        output = [input(361:720,:);input(1:360,:)];
    end
end

end