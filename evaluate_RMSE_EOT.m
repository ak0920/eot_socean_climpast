function [RMSE, RMSEnorm, Count, ModelMeans, Es, normEs] = evaluate_RMSE_EOT(ProxyData, ModelData, Desc)
% [RMSE, RMSEnorm, Count, ModelMeans] = evaluate_RMSE_EOT(ProxyData, ModelData, Desc)
% 
% This function calculates the RMSE metrics for the Kennedy-Asser et al.
% 2020 (Clim. Past) Figure 5 for the EOT time slice.
% 
% Inputs:
%    ProxyData = input data from CSV file for appropriate time slice 
%    ModelData = structure containing all of the required model simulations
%    Desc = the descriptions of the simulations which are to be evaluated 
%       (e.g. Desc = {'end3ficeC',...};
% 
% Outputs:
%    RMSE - structure of standard RMSE values for each model simulation
%    RMSEnorm - structure of normalised RMSE values for each model simulation
%    Count - structure of counts of model simulations within data error 
%       bars for each simulation
%    ModelMeans - n x d array of the modelled mean temperature at each of 
%       the d sites within each time slice
%    Es - n x d array of the error between modelled mean temperature and 
%       the data at each of the d sites within each time slice
%    normEs - n x d array of the error between the normalised modelled mean 
%       temperature and the data normalised at each of the d sites within 
%       each time slice

%% Set some defaults

% Generate area grids
proc_latlon_area % This loads areas_abs
areas_abs = rot90(areas_abs);

% Set column for the maximum/minimum values
maxcol = 6;
mincol = 5;

% Find data locations in model
latsmodel = fliplr(-90:2.5:90);
lonsmodel = -180:3.75:176.25;


%% Find SAT/SST at each location
% Loop through each site, then through each model

% Create an empty array to save mean values in
ModelMeans = nan(length(Desc),height(ProxyData));
Es = nan(length(Desc),height(ProxyData));
normEs = nan(length(Desc),height(ProxyData));
SEs = nan(length(Desc),height(ProxyData));
normSEs = nan(length(Desc),height(ProxyData));

% Create an empty strucutre to do the count in
for j = 1:length(Desc)
    
    % Convert cells (2x CO2 identifiers) into useable string
    desc_cell = Desc(j);
    desc_n = sprintf('%s',desc_cell{:});
    
    % Read the model simulation data into a temporary file
    Count.(desc_n) = 0;
end


%% Find SAT/SST at each location
% Loop through each site, then through each model

% Site names, taken from CSV table
for i = 1:height(ProxyData)
    sitename = sprintf('%s',char(ProxyData{i,1}));
    
    % Find the model location of each site
    [~,sitelat] = min(abs(latsmodel-ProxyData{i,2}));
    [~,sitelon] = min(abs(lonsmodel-ProxyData{i,3}));
    
    % Include � 1 grid cell in lat and long
    sitelats = [sitelat-1, sitelat, sitelat+1];
    
    if sitelon == 1 
        sitelons = [96, 1, 2];
    else
        if sitelon == 96
            sitelons = [95, 96, 1];
        else
            sitelons = [sitelon-1, sitelon, sitelon+1];
        end
    end
    
    
    % Go through each model simulation
    for j = 1:length(Desc)
        
        % Convert cells (2x CO2 identifiers) into useable string
        desc_cell = Desc(j);
        desc_n = sprintf('%s',desc_cell{:});
        
        % Read the model simulation data into a temporary file
        temp = ModelData.(desc_n);
        
        % Mean value from model around site location (store as an array to output/plot later)
        ModelMeans(j,i) = sum(sum(temp(sitelats,sitelons).* areas_abs(sitelats,sitelons)))/sum(sum(areas_abs(sitelats,sitelons)));
        
        % Maximum value from model around site location
        AnnTempMax.(sitename).(desc_n) = max(max(temp(sitelats,sitelons)));
        
        % Minimum value from model around site location
        AnnTempMin.(sitename).(desc_n) = min(min(temp(sitelats,sitelons)));
        
        %% Find E and SE (when there is mean, max and min data available)
        if ~isnan(ProxyData{i,4}) % If site has mean, max and min available

            AnnTempE = ProxyData{i,4}-ModelMeans(j,i); % How do means compare?

            % Find if model lies within proxy/model uncertainty range:
            % Take extremes of proxy and model range
            if AnnTempE > 0 % If model is colder than data...
                AnnTempEmax = ProxyData{i,mincol}-AnnTempMax.(sitename).(desc_n); % ...find difference between model max and data min.
            else
                if AnnTempE < 0 % If model is warmer than data...
                    AnnTempEmax = ProxyData{i,maxcol}-AnnTempMin.(sitename).(desc_n); % ...find difference between model min and data max.
                end
            end
            
            
            % Make new variable, allowing E = 0 if model lies within the proxy range
            Es(j,i) = AnnTempEmax;
            
            % Find if model is colder than mean, but hotter than minimum of
            % data range; then E = 0
            if AnnTempE >= 0 && AnnTempEmax < 0
                Es(j,i) = 0;
                Count.(desc_n) = Count.(desc_n) + 1;
            
            % Find if model is hotter than mean, but colder than maximum of
            % data range; then E = 0
            else
                if AnnTempE <= 0 && AnnTempEmax > 0
                    Es(j,i) = 0;
                    Count.(desc_n) = Count.(desc_n) + 1;

                end
            end
            
            % Calculate squared error
            SEs(j,i) = Es(j,i).^2;

        end
        
        %% Find E and SE when max or min, but not mean data is available:

        % For min only data
        if isnan(ProxyData{i,4}) && ~isnan(ProxyData{i,mincol})
            if AnnTempMax.(sitename).(desc_n) > ProxyData{i,mincol} % is max modelled value > proxy min?
                
                Count.(desc_n) = Count.(desc_n) + 1;
                
                Es(j,i) = 0;
            else % if not, calculate the error
                Es(j,i) = AnnTempMax.(sitename).(desc_n) - ProxyData{i,mincol};
                
            end
            
        % For max only data   
        else
            if isnan(ProxyData{i,4}) && ~isnan(ProxyData{i,maxcol})
                if AnnTempMin.(sitename).(desc_n) < ProxyData{i,maxcol} % is min modelled value < proxy max?
                    
                    Count.(desc_n) = Count.(desc_n) + 1;
                    
                    Es(j,i) = 0;
                    
                else % if not, calculate the error
                    Es(j,i) = AnnTempMin.(sitename).(desc_n) - ProxyData{i,maxcol};
                    
                end
            end
            SEs(j,i) = Es(j,i).^2;
            
        end
        
    end
end


%% Calculate means for proxies and each simulation across all sites
% For the proxy data
SOmeandata = nanmean(ProxyData{:,4});

% For each simulation
for j = 1:length(Desc)
    
    % Convert cells (simulation descriptions) into useable string
    desc_cell = Desc(j);
    desc_n = sprintf('%s',desc_cell{:});
    
    % Go through each model simulation (no area weighting is considered)
    SOmean.(desc_n) = mean(ModelMeans(j,:));
    
end


%% Calculate the 'normalised' error, i.e. with the mean of all sites removed

% Site names, taken from CSV table
for i = 1:height(ProxyData)
    sitename = sprintf('%s',char(ProxyData{i,1}));
        
            % Find the model location of each site
    [~,sitelat] = min(abs(latsmodel-ProxyData{i,2}));
    [~,sitelon] = min(abs(lonsmodel-ProxyData{i,3}));
    
    % Include � 1 grid cell in lat and long
    sitelats = [sitelat-1, sitelat, sitelat+1];
    
    if sitelon == 1 
        sitelons = [96, 1, 2];
    else
        if sitelon == 96
            sitelons = [95, 96, 1];
        else
            sitelons = [sitelon-1, sitelon, sitelon+1];
        end
    end
    

    % Go through each model simulation
    for j = 1:length(Desc)
        
        % Convert cells into useable string
        desc_cell = Desc(j);
        desc_n = sprintf('%s',desc_cell{:});
        
        % Read the model simulation data into a temporary file
        temp = ModelData.(desc_n);
        
        %% Store the data in nested structures:
        
        % Remove SO mean from site mean
        AnnTempMeanNorm.(sitename).(desc_n) = ModelMeans(j,i)-SOmean.(desc_n);
        
        % Maximum value from model around site location
        AnnTempMaxNorm.(sitename).(desc_n) = max(max(temp(sitelats,sitelons)))-SOmean.(desc_n);
        
        % Minimum value from model around site location
        AnnTempMinNorm.(sitename).(desc_n) = min(min(temp(sitelats,sitelons)))-SOmean.(desc_n);
        
        %% Find E and SE (when there is mean, max and min data available)
        if ~isnan(ProxyData{i,4}) % If site has mean, max and min available

            normEs(j,i) = (ProxyData{i,4}-SOmeandata)-AnnTempMeanNorm.(sitename).(desc_n);
            normEs2(j,i) = (ProxyData{i,4}-SOmeandata)-AnnTempMeanNorm.(sitename).(desc_n);
            
            % Find if model lies within proxy/model uncertainty range:
            % Take extremes of proxy and model range
            if normEs2(j,i) > 0 % If model is colder than data...
                AnnTempEmaxNorm = (ProxyData{i,mincol}-SOmeandata)-AnnTempMaxNorm.(sitename).(desc_n); % ...find difference between model max and data min.
            else
                if normEs2(j,i) < 0 % If model is warmer than data...
                    AnnTempEmaxNorm = (ProxyData{i,maxcol}-SOmeandata)-AnnTempMinNorm.(sitename).(desc_n); % ...find difference between model min and data max.
                end
            end
            
            
            % Make new variable, allowing E = 0 if model lies within the proxy range
            normEs(j,i)  = AnnTempEmaxNorm;
            
            % Find if model is colder than mean, but hotter than minimum of
            % data range; then E = 0
            if normEs2(j,i) >= 0 && AnnTempEmaxNorm < 0
                normEs(j,i)  = 0;
%                 Count.(desc_n) = Count.(desc_n) + 1;
            
            % Find if model is hotter than mean, but colder than maximum of
            % data range; then E = 0
            else
                if normEs2(j,i) <= 0 && AnnTempEmaxNorm > 0
                    normEs(j,i)  = 0;
%                     Count.(desc_n) = Count.(desc_n) + 1;

                end
            end
        
        
            
            % E and SE (when there is data available)
            normSEs(j,i) = normEs(j,i).^2;

        end
    end
end


%% Calculate RMSEs
for j = 1:length(Desc)
    
    % Convert cells (simulation descriptions) into useable string
    desc_cell = Desc(j);
    desc_n = sprintf('%s',desc_cell{:});
    
    
    % Calculate RMSE (note: could probably apply an area weighting here,
    % but I have not)
    RMSE.(desc_n) = sqrt(nanmean(SEs(j,:)));

    RMSEnorm.(desc_n) = sqrt(nanmean(normSEs(j,:)));
    
end

