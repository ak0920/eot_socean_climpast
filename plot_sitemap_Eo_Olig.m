% plot_sitemap_Eo_Olig.m
% 
% This script plots Figure 1 of Kennedy-Asser et al. 2020 (Clim. Past): a
% map of proxy locations and records for the Eocene and Oligocene. The
% legend was added manually with image editing software and text locations
% need to be manually tweaked to improve legibility.
% 

%% Load data

% Load topograpy (creating by merging Rupelian topography and bathymetry 
% from Getech, used in Kennedy-Asser et al. 2019; Paleoceanography and
% Paleoclimatology)
load('RupTopo.mat')

% Load records
load_proxies

% Colours and figure defaults
red = [0.850000000000000,0.0872549042105675,0];
blue = [0.0585574759170413,0.364924505352974,0.679114878177643];
grey = [0.6 0.6 0.6];
lgrey = [0.8 0.8 0.8];
font = 'Arial';
fontsz = 11;


%% Generate lat lon grids for plotting
% For lat long
latlim = [-90 0]; 
lonlim = [-180 180];
[gridxlow,gridylow] = meshgrid([0:3.75:180,-176.25:3.75:0],-90:2.5:90);
[gridxlow2,gridylow2] = meshgrid(-180:3.75:180,-90:2.5:90);


%% Plot base map
% Set up figure
figure
subplot('Position', [0.2 0.2 0.6 0.6])
colormap('gray')
caxis([-24000 4000])
axesm('MapProjection','ortho','origin',[-90 0],'maplatlim',[-90 -40],'Grid','on', ...
    'GLineWidth',1,'GLineStyle','-',...
    'Gcolor',[0.3 0.3 0.3],'Galtitude',1);%100);

% Plot topo base map
contourm(gridylow(1:73,:),gridxlow(1:73,:),RupTopo(1:73,:),-24000:1000:4000,'Fill','on')

% add coastline
hold on
contourm(gridylow,gridxlow,RupTopo, [0 0], 'k-','linewidth',2); 

% Add sites
box off
gridm('reset')

sitelabs = {'Maud Rise','Prydz Bay','Kerguelen Plateau','U1356','U1360',...
'S. Australia','E. Tasman Plateau','Ross Sea','New Zealand','King George Isl.',...
'Seymour Isl.','Weddell Sea','Falklands Plateau'};

sitelats = [-65.3 -66.2 -58.6 -61.1 -66.3 -54.4 -60.7 -76.7 -59.4 -63.8 -66.3 -64.0 -53.1];
sitelons = [1.6 73.0 79.8 130.3 136.8 144.8 152.9 155.9 176.6 -62.6 -58.4 -40.7 -38.6];

plotm([-70 -70 -50 -50 -70],[-70 -50 -50 -70 -70],'-','color',red,'linewidth',2)
plotm(sitelats,sitelons,'p','markersize',20,'MarkerFaceColor',red,'MarkerEdgeColor','k')
textm(sitelats, sitelons, sitelabs, 'fontname',font,'fontsize',fontsz)
textm(-60, -60, 'S.W. Atlantic', 'fontname',font,'fontsize',fontsz)


% Tidy up
set(gcf,'position',[0 0 700 700],'units','normalized')
set(gca,'position',[0.2 0.2 0.6 0.6],'units','normalized')
set(gcf, 'color', 'w');

% Get rid of annoying x-y axis around map
axes('position',[0 0 0.21 1])
fill([0 0 1 1 0],[0 1 1 0 0],'w','edgecolor','none')
axis off
axes('position',[0 0 1 0.21])
fill([0 0 1 1 0],[0 1 1 0 0],'w','edgecolor','none')
axis off


%% Add data subplots

ylims = [4 28];

% Maud Rise
axes('position', [0.65 0.85 0.125 0.125])
site_name = 'MaudRise';
proxy_data_subplot
title('Maud Rise ODP 689','fontweight','normal')


% Prydz Bay
axes('position', [0.85 0.85 0.125 0.125])
site_name = 'PrydzBay';
proxy_data_subplot
title('Prydz Bay (multiple)','fontweight','normal')


% Kerguelen Plateau
axes('position', [0.85 0.65 0.125 0.125])
site_name = 'KerguelenPlateau';
proxy_data_subplot
title('Kerguelen Plateau (multiple)','fontweight','normal')


% Wilkes Land U1356
axes('position', [0.85 0.45 0.125 0.125])
site_name = 'WilkesU1356';
proxy_data_subplot
title('Wilkes Land U1356','fontweight','normal')


% Wilkes Land U1360
axes('position', [0.85 0.25 0.125 0.125])
site_name = 'WilkesU1360';
proxy_data_subplot
title('Wilkes Land U1360','fontweight','normal')


% S. Australia
axes('position', [0.85 0.05 0.125 0.125])
site_name = 'SAustralia';
proxy_data_subplot
title('South Australia','fontweight','normal')


% East Tasman Plateau
axes('position', [0.65 0.05 0.125 0.125])
site_name = 'EastTasmanPlateau';
proxy_data_subplot
title('E. Tasman Plateau ODP 1172','fontweight','normal')


% New Zealand
axes('position', [0.45 0.05 0.125 0.125])
site_name = 'NewZealand';
proxy_data_subplot
title('New Zealand DSDP 277','fontweight','normal')


% Ross Sea
axes('position', [0.05 0.25 0.125 0.125])
site_name = 'RossSea';
proxy_data_subplot
title('Ross Sea (multiple)','fontweight','normal')


% King George Island
axes('position', [0.05 0.45 0.125 0.125])
site_name = 'KingGeorgeIsl';
proxy_data_subplot
title('King George Isl.','fontweight','normal')


% Seymour Island
axes('position', [0.05 0.65 0.125 0.125])
site_name = 'SeymourIsl';
proxy_data_subplot
title('Seymour Isl.','fontweight','normal')


% Weddell Sea
axes('position', [0.05 0.85 0.125 0.125])
site_name = 'WeddellSea';
proxy_data_subplot
title('Weddell Sea ODP 696','fontweight','normal')


% SW Atlantic
axes('position', [0.25 0.85 0.125 0.125])
site_name = 'SWAtlantic';
proxy_data_subplot
title('South West Atlantic region','fontweight','normal')


% Falklands Plateau
axes('position', [0.45 0.85 0.125 0.125])
site_name = 'FalklandsPlateau';
proxy_data_subplot
title('Falklands Plateau ODP 511','fontweight','normal')

