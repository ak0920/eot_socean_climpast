% load_latgrads.m
% 
% These are the values for the lat. grad. benchmark for each proxy dataset.
% Usually they are read in as in older versions of Matlab, polyfit caused
% it to crash. They can be calculated from scratch by uncommenting the
% lower part of the script.
% 

Eocenem = 0.4877; Eocenec = 46.0300;
Oligocenem = 0.5386; Oligocenec = 46.6921;
EOTm = -0.1972; EOTc = -14.6675;

ms = [0.4877 0.5386 -0.1972];
cs = [46.0300 46.6921 -14.6675];


%% These are generated using the following:
% % Uncomment to print values for each dataset
% load_proxies
% for i = 1:3
%     if i ==1
%         Data = EoceneTemp;
%     else
%         if i ==2
%             Data = OligoceneTemp;
%         else
%             Data = EOTTemp;
%         end
%     end
%     
%     idx = isnan(Data{:,4});
%     p = polyfit(Data{~idx,2},Data{~idx,4},1);
%     disp(p) 
% end