% evaluateEOT.m
% 
% Carry out the evaluation of each simulation compared to EOT data and
% plot Figure 5 of Kennedy-Asser et al. 2020 (Clim. Past).
% 

clear

%% Load the data
% HadCM3BL
load_HadCM3BL

% FOAM
load_FOAM

% Proxy data
load_proxies

% Load latitudinal gradient data
load_latgrads

% Load change across EOT
load_Temp_response

% Create benchmark data
TempEOT.bm_const = nanmean(EOTTemp{:,4})*ones(73,96);
TempEOT.bm_latgr = repmat(flipud(lats)*(EOTm)+EOTc,1,96); % Gradient and intercept from LatGrads.xlsx 


%% HadCM3BL first 

% Calculate RMSEs
desc = {'end3ficeC','end3ficeO','end2ficeC','end2ficeO',...
    'endfCO2C','endfCO2O',...
    'end3fgeog','end2fgeog','endall',...
    'bm_const','bm_latgr'};

[EOTRMSE, EOTRMSEnorm, EOTCount, EOTModelMeans,EOTEs,EOTnormEs] = evaluate_RMSE_EOT(EOTTemp,TempEOT,desc);
[EOTCountDir] = evaluate_dirchange(EOTTemp,TempEOT,desc);


%% Convert structures to arrays for plotting

for j = 1:length(desc)
    
    % Convert cells (simulation descriptions) into useable string
    desc_cell = desc(j);
    desc_n = sprintf('%s',desc_cell{:});
    
    
    % Calculate RMSE (note: could probably apply an area weighting here,
    % but I have not)
    RMSEplot(j) = EOTRMSE.(desc_n);

    RMSEnormplot(j) = EOTRMSEnorm.(desc_n);
    
    CountPlot(j) = EOTCount.(desc_n);
    
end


%% Prepare to plot
load('warm_cols.mat')
clim = [0 4];
font = 'Arial';
fontsz = 14;

% Find how many records there are (to normalise the count metrics /1)
nodata=sum(strcmp(EOTTemp{:,8},'nodata'))+sum(strcmp(EOTTemp{:,8},'notclear'))+sum((strcmp(EOTTemp{:,8},'Dinocysts') + ~isnan(EOTTemp{:,5}))>1);
CountNorm = max(clim)*(height(EOTTemp)-nodata-CountPlot)/(height(EOTTemp)-nodata);
CountDirNorm = max(clim)*(height(EOTTemp)-nodata-EOTCountDir)/(height(EOTTemp)-nodata);

% Put variables to plot into an array
plotting = [RMSEplot;RMSEnormplot;CountNorm;CountDirNorm];

ydesc = {'Standard RMSE (�C)', 'Normalised RMSE (�C)', ['Count (within data error bars /',num2str(height(EOTTemp)-nodata),')'],...
    ['Count (correct dir. of change /',num2str(height(EOTTemp)-nodata),')']};
xdesc = {'3x pCO_2, closed DP (EAIS)','3x pCO_2, open DP (EAIS)','2x pCO_2, closed DP (EAIS)','2x pCO_2, open DP (EAIS)',...
    'closed DP (EAIS, 3-2x pCO_2)','open DP (EAIS, 3-2x pCO_2)',...
    '3x pCO_2 (EAIS, DP opening)','2x pCO_2 (EAIS, DP opening)','(EAIS, 3-2x pCO_2, DP opening)'...    
    'Constant mean','Latitudinal gradient'};


%% Plot
figure
subplot(2,1,1)
imagesc(plotting)
colormap(warm_cols)
caxis(clim)

% Add lines between model groups/forcings
hold on
plot([4.5 4.5],[0.5 4.5],'-k','linewidth',2)
plot([6.5 6.5],[0.5 4.5],'-k','linewidth',2)
plot([8.5 8.5],[0.5 4.5],'-k','linewidth',2)
plot([9.5 9.5],[0.5 4.5],'-k','linewidth',2)
plot([0.5 43.5],[1.5 1.5],'-k')
plot([0.5 43.5],[2.5 2.5],'-k')
plot([0.5 43.5],[3.5 3.5],'-k')

% Add values for each metric
for i = 1:length(xdesc)
    text(i,4,sprintf('%.0f',EOTCountDir(1,i)),'HorizontalAlignment','center', 'fontname', font, 'fontsize', fontsz)
    text(i,3,sprintf('%.0f',CountPlot(1,i)),'HorizontalAlignment','center', 'fontname', font, 'fontsize', fontsz)
    text(i,2,sprintf('%.1f',RMSEnormplot(1,i)),'HorizontalAlignment','center', 'fontname', font, 'fontsize', fontsz)
    text(i,1,sprintf('%.1f',RMSEplot(1,i)),'HorizontalAlignment','center', 'fontname', font, 'fontsize', fontsz)
end

% Tidy up
set(gca, 'XTick',1:length(xdesc), 'XTickLabel',xdesc, 'YTick',1:length(ydesc), 'YTickLabel',ydesc, 'fontname', font, 'fontsize', fontsz)
xtickangle(35)
axis equal
xlim([0.5 11.5])
ylim([0.5 4.5])

text(2.5,0,'AIS growth','HorizontalAlignment','center', 'fontname', font, 'fontsize', fontsz)
text(5.35,-0.5,'AIS growth +','HorizontalAlignment','center', 'fontname', font, 'fontsize', fontsz)
text(5.35,0,' pCO_2 drop','HorizontalAlignment','center', 'fontname', font, 'fontsize', fontsz)
text(7.65,-0.5,'AIS growth +','HorizontalAlignment','center', 'fontname', font, 'fontsize', fontsz)
text(7.65,0,'DP opening','HorizontalAlignment','center', 'fontname', font, 'fontsize', fontsz)
text(9,0,'All','HorizontalAlignment','center', 'fontname', font, 'fontsize', fontsz)
text(10.5,0,'Benchmarks','HorizontalAlignment','center', 'fontname', font, 'fontsize', fontsz)
text(-4.5,0,'a','HorizontalAlignment','center','fontweight','bold', 'fontname', font, 'fontsize', 16)


%% Add indicators for moderate or good performance
% These are added manually

% Moderate performance (1 star)
xmod = [1 3 6];
ymod = [1 1 1];
plot(xmod,ymod+0.35,'pk')

%% Tidy up
c = colorbar;
ylabel(c, '(Normalised) RMSE (�C)', 'fontname', font, 'fontsize', fontsz)
set(c, 'fontname', font, 'fontsize', fontsz) 

set(gcf, 'color', 'w');

%% FOAM second 

% Calculate RMSEs
desc = {'Fsice2','Feice2','Ffice2','Fsice2c','Feice2c','Ffice2c',...
    'FsCO2','FeCO2','FfCO2','FsCO22','FeCO22','FfCO22','FsCO2c','FeCO2c','FfCO2c','FsCO22c','FeCO22c','FfCO22c'};

[EOTRMSE, EOTRMSEnorm, EOTCount, EOTModelMeans,EOTEs,EOTnormEs] = evaluate_RMSE_EOT(EOTTemp,TempEOT,desc);
[EOTCountDir] = evaluate_dirchange(EOTTemp,TempEOT,desc);


%% Convert structures to arrays for plotting

for j = 1:length(desc)
    
    % Convert cells (simulation descriptions) into useable string
    desc_cell = desc(j);
    desc_n = sprintf('%s',desc_cell{:});
    
    % Calculate RMSE (note: could probably apply an area weighting here,
    % but I have not)
    RMSEplot(j) = EOTRMSE.(desc_n);

    RMSEnormplot(j) = EOTRMSEnorm.(desc_n);
    
    CountPlot(j) = EOTCount.(desc_n);
    
end


%% Prepare to plot
load('warm_cols.mat')
clim = [0 4];
font = 'Arial';
fontsz = 14;

nodata=sum(strcmp(EOTTemp{:,8},'nodata'))+sum(strcmp(EOTTemp{:,8},'notclear'))+sum((strcmp(EOTTemp{:,8},'Dinocysts') + ~isnan(EOTTemp{:,5}))>1);
CountNorm = max(clim)*(height(EOTTemp)-nodata-CountPlot)/(height(EOTTemp)-nodata);
CountDirNorm = max(clim)*(height(EOTTemp)-nodata-EOTCountDir)/(height(EOTTemp)-nodata);

plotting = [RMSEplot;RMSEnormplot;CountNorm;CountDirNorm];

ydesc = {'Standard RMSE (�C)', 'Normalised RMSE (�C)', ['Count (within data error bars /',num2str(height(EOTTemp)-nodata),')'],...
    ['Count (correct dir. of change /',num2str(height(EOTTemp)-nodata),')']};
xdesc = {'2x pCO_2, warm orbit (small EAIS)','2x pCO_2, warm orbit (EAIS)','2x pCO_2, warm orbit (full AIS)',...
    '2x pCO_2, cold orbit (small EAIS)','2x pCO_2, cold orbit (EAIS)','2x pCO_2, cold orbit (full AIS)',...
    'Warm orbit (small EAIS + 3-2x)','Warm orbit (EAIS + 3-2x)','Warm orbit (full AIS + 3-2x)','Warm orbit (small EAIS + 4-2x)','Warm orbit (EAIS + 4-2x)','Warm orbit (full AIS growth + 4-2x)'...
    'Cold orbit (small EAIS + 3-2x)','Cold orbit (EAIS + 3-2x)','Cold orbit (full AIS + 3-2x)','Cold orbit (small EAIS + 4-2x)','Cold orbit (EAIS + 4-2x)','Cold orbit (full AIS growth + 4-2x)'};


%% Plot
subplot(2,1,2)
imagesc(plotting)
colormap(warm_cols)
caxis(clim)

hold on
plot([6.5 6.5],[0.5 4.5],'-k','linewidth',2)
plot([0.5 43.5],[1.5 1.5],'-k')
plot([0.5 43.5],[2.5 2.5],'-k')
plot([0.5 43.5],[3.5 3.5],'-k')

for i = 1:length(xdesc)
    text(i,4,sprintf('%.0f',EOTCountDir(1,i)),'HorizontalAlignment','center', 'fontname', font, 'fontsize', fontsz)
    text(i,3,sprintf('%.0f',CountPlot(1,i)),'HorizontalAlignment','center', 'fontname', font, 'fontsize', fontsz)
    text(i,2,sprintf('%.1f',RMSEnormplot(1,i)),'HorizontalAlignment','center', 'fontname', font, 'fontsize', fontsz)
    text(i,1,sprintf('%.1f',RMSEplot(1,i)),'HorizontalAlignment','center', 'fontname', font, 'fontsize', fontsz)
end

% Tidy up
set(gca, 'XTick',1:length(xdesc), 'XTickLabel',xdesc, 'YTick',1:length(ydesc), 'YTickLabel',ydesc, 'fontname', font, 'fontsize', fontsz)
xtickangle(35)
axis equal
xlim([0.5 18.5])
ylim([0.5 4.5])

text(3.5,0,'AIS growth','HorizontalAlignment','center', 'fontname', font, 'fontsize', fontsz)
text(12.5,0,'AIS growth + pCO_2 drop','HorizontalAlignment','center', 'fontname', font, 'fontsize', fontsz)
text(-4.5,0,'b','HorizontalAlignment','center','fontweight','bold', 'fontname', font, 'fontsize', 16)


%% Add indicators for moderate or good performance
% Moderate performance (1 star)
xmod = [];
ymod = [];
plot(xmod,ymod+0.35,'pk')


%% Tidy up
c = colorbar;
ylabel(c, '(Normalised) RMSE (�C)', 'fontname', font, 'fontsize', fontsz)
set(c, 'fontname', font, 'fontsize', fontsz) 

set(gcf, 'color', 'w');
set(gcf,'position',[0 0 1100 700],'units','normalized')
