% load_Temp_response_DJF.m
% 
% Finds the difference between all model siumaltions that make up
% 'EOT-like' pairs, i.e. include ice growth and potentially another
% forcing, saved in a structure called TempEOTdjf. This is done manually
% because it is easier.
% 
% This is the summer (DJF) version of the script
% 

%% Load the simulation data
load_HadCM3BL
load_FOAM


%% Find the differences

% Ice expansion only
TempEOTdjf.end3ficeO = Tempdjf.end3fO - Tempdjf.end3nO;
TempEOTdjf.end2ficeO = Tempdjf.end2fO - Tempdjf.end2nO;
TempEOTdjf.end3ficeC = Tempdjf.end3fC - Tempdjf.end3nC;
TempEOTdjf.end2ficeC = Tempdjf.end2fC - Tempdjf.end2nC;

TempEOTdjf.Fsice2 = FoamSAT_AIS1_DJF_i - FoamSAT2n_DJF_i;
TempEOTdjf.Feice2 = FoamSAT_AIS2_DJF_i - FoamSAT2n_DJF_i;
TempEOTdjf.Ffice2 = FoamSAT_AIS3_DJF_i - FoamSAT2n_DJF_i;
TempEOTdjf.Fsice2c = FoamSAT_AIS1c_DJF_i - FoamSAT2nc_DJF_i;
TempEOTdjf.Feice2c = FoamSAT_AIS2c_DJF_i - FoamSAT2nc_DJF_i;
TempEOTdjf.Ffice2c = FoamSAT_AIS3c_DJF_i - FoamSAT2nc_DJF_i;


% pCO2 (+ ice)
TempEOTdjf.endfCO2O = Tempdjf.end2fO - Tempdjf.end3nO;
TempEOTdjf.endfCO2C = Tempdjf.end2fC - Tempdjf.end3nC;

TempEOTdjf.FsCO2 = FoamSAT_AIS1_DJF_i - FoamSAT3n_DJF_i;
TempEOTdjf.FeCO2 = FoamSAT_AIS2_DJF_i - FoamSAT3n_DJF_i;
TempEOTdjf.FfCO2 = FoamSAT_AIS3_DJF_i - FoamSAT3n_DJF_i;
TempEOTdjf.FsCO22 = FoamSAT_AIS1_DJF_i - FoamSAT4n_DJF_i;
TempEOTdjf.FeCO22 = FoamSAT_AIS2_DJF_i - FoamSAT4n_DJF_i;
TempEOTdjf.FfCO22 = FoamSAT_AIS3_DJF_i - FoamSAT4n_DJF_i;
TempEOTdjf.FsCO2c = FoamSAT_AIS1c_DJF_i - FoamSAT3nc_DJF_i;
TempEOTdjf.FeCO2c = FoamSAT_AIS2c_DJF_i - FoamSAT3nc_DJF_i;
TempEOTdjf.FfCO2c = FoamSAT_AIS3c_DJF_i - FoamSAT3nc_DJF_i;
TempEOTdjf.FsCO22c = FoamSAT_AIS1c_DJF_i - FoamSAT4nc_DJF_i;
TempEOTdjf.FeCO22c = FoamSAT_AIS2c_DJF_i - FoamSAT4nc_DJF_i;
TempEOTdjf.FfCO22c = FoamSAT_AIS3c_DJF_i - FoamSAT4nc_DJF_i;


% Palaeogeog. (+ ice)
TempEOTdjf.end3fgeog = Tempdjf.end3fO - Tempdjf.end3nC;
TempEOTdjf.end2fgeog = Tempdjf.end2fO - Tempdjf.end2nC;


% Palaeogeog. + pCO2 (+ ice)
TempEOTdjf.endall = Tempdjf.end2fO - Tempdjf.end3nC;

