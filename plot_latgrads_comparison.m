% plot_latgrads_comparison.m
%
% Plot the proxy data for each time slice against latitude in comparison
% with the best HadCM3BL and FOAM simulations.
% Also adds a best fit line for the proxy data.
%
% For creation of Figure 3 in Kennedy-Asser et al. 2020 (Clim. Past)
% 

%% Load data
load_proxies
load_Temp_response

% Load colours and set some image defaults
red = [0.850000000000000,0.0872549042105675,0];
orange = [1,0.400000005960465,0.200000002980232];
green = [0.530028571428571,0.749114285714286,0.466114285714286];
blue = [0.0585574759170413,0.364924505352974,0.679114878177643];
grey = [0.6 0.6 0.6];
lgrey = [0.8 0.8 0.8];
font = 'Arial';
fontsz = 14;
xlims = [-80 -50];
ylims = [-15 30];

close('all')

%% Start to plot
figure

% Repeat the same procedure for each dataset/time slice
for i = 1:3
    % Select correct dataset for each subplot
    if i ==1
        Data = EoceneTemp;
    else
        if i ==2
            Data = OligoceneTemp;
        else
            Data = EOTTemp;
        end
    end
    
    
    % Generate ranges for plotting error bars
    rangelats = nan;
    ranges = nan;
    
    for j = 1:height(Data)
        if ~isnan(Data{j,5}) && ~isnan(Data{j,6})
            
            rangelats(length(rangelats)+1) = Data{j,2};
            rangelats(length(rangelats)+1) = Data{j,2};
            rangelats(length(rangelats)+1) = nan;
            
            ranges(length(ranges)+1) = Data{j,5};
            ranges(length(ranges)+1) = Data{j,6};
            ranges(length(ranges)+1) = nan;
        end
    end
    
    
    %% Plot figures
    subplot(1,3,i)
    hold on
    if i == 1
        % Plot best models
        plot(Hadlat(5:17),flipud(mean(FoamSAT4n_i(57:69,:),2)),'-','color',red,'linewidth',2)
        plot(Hadlat(5:17),flipud(mean(Temp.end3nO(57:69,:),2)),'-','color',blue,'linewidth',2)
        
        % Plot proxy data points
        plot(Data{:,2},Data{:,6},'^','color',grey,'markersize',10,'markerfacecolor',grey)
        plot(Data{:,2},Data{:,5},'v','color',grey,'markersize',10,'markerfacecolor',grey)
        
        % Tidy up
        ylim([-20 30])
        text(-53,-17,'a', 'fontname',font,'fontsize',16,'fontweight','bold')
        text(-65,-17,['n = ',num2str(sum(~isnan(Data{:,4})))], 'fontname',font,'fontsize',fontsz,'HorizontalAlignment','center');

    else
        if i ==2
            % Plot best models
            plot(Hadlat(5:17),flipud(mean(FoamSAT_AIS1_i(57:69,:),2)),'-','color',red,'linewidth',2)
            plot(Hadlat(5:17),flipud(mean(Temp.end3fO(57:69,:),2)),'-','color',blue,'linewidth',2)
            
            % Plot proxy data records
            plot(Data{:,2},Data{:,6},'^','color',grey,'markersize',10,'markerfacecolor',grey)
            plot(Data{:,2},Data{:,5},'v','color',grey,'markersize',10,'markerfacecolor',grey)
            
            % Tidy up
            ylim([-20 30])
            text(-53,-17,'b', 'fontname',font,'fontsize',16,'fontweight','bold')
            text(-65,-17,['n = ',num2str(sum(~isnan(Data{:,4})))], 'fontname',font,'fontsize',fontsz,'HorizontalAlignment','center');

        else
            % Plot best models
            plot(Hadlat(5:17),flipud(mean(TempEOT.FsCO22(57:69,:),2)),'-','color',red,'linewidth',2)
            plot(Hadlat(5:17),flipud(mean(TempEOT.end3ficeO(57:69,:),2)),'-','color',blue,'linewidth',2)
            
            % Plot proxy data records
            plot(Data{:,2},Data{:,5},'^','color',grey,'markersize',10,'markerfacecolor',grey)
            plot(Data{:,2},Data{:,6},'v','color',grey,'markersize',10,'markerfacecolor',grey)
            
            % Tidy up
            ylim([-30 5])
            text(-53,-27.5,'c', 'fontname',font,'fontsize',16,'fontweight','bold')
            text(-65,-27,['n = ',num2str(sum(~isnan(Data{:,4})))], 'fontname',font,'fontsize',fontsz,'HorizontalAlignment','center');

        end
    end
    
    % Add mean change benchmark line
    plot(xlims,[nanmean(Data{:,4}) nanmean(Data{:,4})],'--','color',grey,'linewidth',2)
    plot(Data{:,2},Data{:,4},'o','color','k','markersize',10,'markerfacecolor','k')
    
    % Tidy up
    xlim(xlims)
    xticks([-80 -70 -60 -50]);xticklabels({'80' '70' '60' '50'});
    box on
    if i == 3
        ylabel('SAT change �C', 'fontname',font,'fontsize',fontsz)
        gradlabely = -25;
    else
        ylabel('SAT �C', 'fontname',font,'fontsize',fontsz)
        gradlabely = -13.5;
    end
    xlabel('Latitude �S', 'fontname',font,'fontsize',fontsz)
    set(gca, 'fontname',font,'fontsize',fontsz)
    
    % Add lat. grad. benchmark best fit line
    idx = isnan(Data{:,4});
    p = polyfit(Data{~idx,2},Data{~idx,4},1);
    f = polyval(p,Data{~idx,2});
    if i ~= 3
        plot([min(Data{~idx,2}) max(Data{~idx,2})],[min(f) max(f)],'--k','linewidth',2)
    else
        plot([max(Data{~idx,2}) min(Data{~idx,2})],[min(f) max(f)],'--k','linewidth',2)
    end
    text(-65,gradlabely,['Lat. grad. = ',num2str(p(1))], 'fontname',font,'fontsize',fontsz,'HorizontalAlignment','center');
    
    % Add error bars between max/min ranges
    plot(rangelats,ranges,'-','color',grey,'linewidth',3)
    % Add mid points/mean values
    plot(Data{:,2},Data{:,4},'o','color','k','markersize',10,'markerfacecolor','k')

end

% Tidy up
set(gcf, 'color', 'w');
set(gcf,'position',[0 0 1200 350],'units','normalized')
