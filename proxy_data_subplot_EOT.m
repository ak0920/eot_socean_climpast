% proxy_data_subplot_EOT.m
% 
% This script subplots the proxy records around the map for Figure 2 of 
% Kennedy-Aser et al. 2020 (Clim. Past). site_name fust be dfined as a 
% string before running it, e.g. 'MaudRise', as it is defined in the CSV file.
% 
% Also remember to change the subplot location and give the subplot a title.
% 

hold on

% Take data from table
x = sum(strcmp(EOTTemp{:,1},site_name));
xx = 1:x;
ymean = EOTTemp{strcmp(EOTTemp{:,1},site_name),4};
ymax = EOTTemp{strcmp(EOTTemp{:,1},site_name),5};
ymin = EOTTemp{strcmp(EOTTemp{:,1},site_name),6};
yprox = EOTTemp{strcmp(EOTTemp{:,1},site_name),8};

for i = 1:x
    if isnan(ymin(i))
        plot(i,ymax(i),'^k','markersize',10,'markerfacecolor','k')
    else
        if isnan(ymax(i))
            plot(i,ymin(i),'vk','markersize',10,'markerfacecolor','k')
        else
            plot([i i],[ymax(i) ymin(i)],'-k','linewidth',3)
            plot(i,ymin(i),'vk','markersize',10,'markerfacecolor','k')
            plot(i,ymax(i),'^k','markersize',10,'markerfacecolor','k')
        end
    end
        
    plot(i,ymean(i),'.k','markersize',30)
end

% Tidy figure
xlim([0 x+1])
ylim(ylims)
set(gca, 'XTick',xx, 'XTickLabel',{yprox{:}}, 'fontname',font,'fontsize',10)

if x > 1
    xtickangle(55)
end
ylabel('SAT change (�C)')

if strcmp(yprox{x},'nodata')
    text(1,1,'No data','HorizontalAlignment','center')
    set(gca, 'XTick',[], 'YTick',[],'xlim', [0 2],'ylim',[0 2])
else
    if strcmp(yprox{x},'notclear')
        text(1,1,'No clear change','HorizontalAlignment','center')
        set(gca, 'XTick',[], 'YTick',[],'xlim', [0 2],'ylim',[0 2])
    end
end

% Tidy figure
ax = gca;
ax.YGrid = 'on';
box on
