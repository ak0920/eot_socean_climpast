% load_FOAM.m
% 
% Load the FOAM data used in Kennedy-Asser et al. 2020 (Clim. Past).
% Simulations were carried out by J.B. Ladant and were those used in Ladant
% et al. 2014 (Paleoceanography).
% 
% This script interpolates the other models onto the HadCM3BL grid to make
% other processing easier (e.g. proxy locations).
% 
% IMPORTANT NOTE:
% This paper was built upon work for my PhD thesis which included other
% climate models, some of which were atmosphere only. For that reason, the
% FOAM data obtained and used is only for the atmosphere, however the
% impact this is expected to have over the ocean is expected to be
% relatively small, as SAT and SST are generally similar.
% 


%% Load FOAM data
FoamSATAIS1 = ncread('../FOAM/FOAM.34Ma.WAM.2x.WSO.ISS1.atm.nc','TEMP',[1 1 18 1],[Inf Inf 1 Inf])-273.15;
FoamSATAIS2 = ncread('../FOAM/FOAM.34Ma.WAM.2x.WSO.ISS2.atm.nc','TEMP',[1 1 18 1],[Inf Inf 1 Inf])-273.15;
FoamSATAIS3 = ncread('../FOAM/FOAM.34Ma.WAM.2x.WSO.ISS3.atm.nc','TEMP',[1 1 18 1],[Inf Inf 1 Inf])-273.15;
FoamSAT2n = ncread('../FOAM/FOAM.34Ma.WAM.2x.WSO.atm.nc','TEMP',[1 1 18 1],[Inf Inf 1 Inf])-273.15;
FoamSAT3n = ncread('../FOAM/FOAM.34Ma.WAM.3x.WSO.atm.nc','TEMP',[1 1 18 1],[Inf Inf 1 Inf])-273.15;
FoamSAT4n = ncread('../FOAM/FOAM.34Ma.WAM.4x.WSO.atm.nc','TEMP',[1 1 18 1],[Inf Inf 1 Inf])-273.15;

FoamSATAIS1c = ncread('../FOAM/FOAM.34Ma.WAM.2x.CSO.ISS1.atm.nc','TEMP',[1 1 18 1],[Inf Inf 1 Inf])-273.15;
FoamSATAIS2c = ncread('../FOAM/FOAM.34Ma.WAM.2x.CSO.ISS2.atm.nc','TEMP',[1 1 18 1],[Inf Inf 1 Inf])-273.15;
FoamSATAIS3c = ncread('../FOAM/FOAM.34Ma.WAM.2x.CSO.ISS3.atm.nc','TEMP',[1 1 18 1],[Inf Inf 1 Inf])-273.15;
FoamSAT2nc = ncread('../FOAM/FOAM.34Ma.WAM.2x.CSO.atm.nc','TEMP',[1 1 18 1],[Inf Inf 1 Inf])-273.15;
FoamSAT3nc = ncread('../FOAM/FOAM.34Ma.WAM.3x.CSO.atm.nc','TEMP',[1 1 18 1],[Inf Inf 1 Inf])-273.15;
FoamSAT4nc = ncread('../FOAM/FOAM.34Ma.WAM.4x.CSO.atm.nc','TEMP',[1 1 18 1],[Inf Inf 1 Inf])-273.15;


% Select summer only
FoamSATAIS1_DJF = rot90(mean(FoamSATAIS1(:,:,[12 1 2]),3));
FoamSATAIS2_DJF = rot90(mean(FoamSATAIS2(:,:,[12 1 2]),3));
FoamSATAIS3_DJF = rot90(mean(FoamSATAIS3(:,:,[12 1 2]),3));
FoamSAT2n_DJF = rot90(mean(FoamSAT2n(:,:,[12 1 2]),3));
FoamSAT3n_DJF = rot90(mean(FoamSAT3n(:,:,[12 1 2]),3));
FoamSAT4n_DJF = rot90(mean(FoamSAT4n(:,:,[12 1 2]),3));
FoamSATAIS1c_DJF = rot90(mean(FoamSATAIS1c(:,:,[12 1 2]),3));
FoamSATAIS2c_DJF = rot90(mean(FoamSATAIS2c(:,:,[12 1 2]),3));
FoamSATAIS3c_DJF = rot90(mean(FoamSATAIS3c(:,:,[12 1 2]),3));
FoamSAT2nc_DJF = rot90(mean(FoamSAT2nc(:,:,[12 1 2]),3));
FoamSAT3nc_DJF = rot90(mean(FoamSAT3nc(:,:,[12 1 2]),3));
FoamSAT4nc_DJF = rot90(mean(FoamSAT4nc(:,:,[12 1 2]),3));


% Take annual mean
FoamSATAIS1 = rot90(mean(squeeze(FoamSATAIS1),3));
FoamSATAIS2 = rot90(mean(squeeze(FoamSATAIS2),3));
FoamSATAIS3 = rot90(mean(squeeze(FoamSATAIS3),3));
FoamSAT2n = rot90(mean(squeeze(FoamSAT2n),3));
FoamSAT3n = rot90(mean(squeeze(FoamSAT3n),3));
FoamSAT4n = rot90(mean(squeeze(FoamSAT4n),3));
FoamSATAIS1c = rot90(mean(squeeze(FoamSATAIS1c),3));
FoamSATAIS2c = rot90(mean(squeeze(FoamSATAIS2c),3));
FoamSATAIS3c = rot90(mean(squeeze(FoamSATAIS3c),3));
FoamSAT2nc = rot90(mean(squeeze(FoamSAT2nc),3));
FoamSAT3nc = rot90(mean(squeeze(FoamSAT3nc),3));
FoamSAT4nc = rot90(mean(squeeze(FoamSAT4nc),3));


% Repeat final longitude band (necessary for interpolating later)
FoamSATAIS1(:,49) = FoamSATAIS1(:,1);
FoamSATAIS2(:,49) = FoamSATAIS2(:,1);
FoamSATAIS3(:,49) = FoamSATAIS3(:,1);
FoamSAT2n(:,49) = FoamSAT2n(:,1);
FoamSAT3n(:,49) = FoamSAT3n(:,1);
FoamSAT4n(:,49) = FoamSAT4n(:,1);
FoamSATAIS1c(:,49) = FoamSATAIS1c(:,1);
FoamSATAIS2c(:,49) = FoamSATAIS2c(:,1);
FoamSATAIS3c(:,49) = FoamSATAIS3c(:,1);
FoamSAT2nc(:,49) = FoamSAT2nc(:,1);
FoamSAT3nc(:,49) = FoamSAT3nc(:,1);
FoamSAT4nc(:,49) = FoamSAT4nc(:,1);
FoamSATAIS1_DJF(:,49) = FoamSATAIS1_DJF(:,1);
FoamSATAIS2_DJF(:,49) = FoamSATAIS2_DJF(:,1);
FoamSATAIS3_DJF(:,49) = FoamSATAIS3_DJF(:,1);
FoamSAT2n_DJF(:,49) = FoamSAT2n_DJF(:,1);
FoamSAT3n_DJF(:,49) = FoamSAT3n_DJF(:,1);
FoamSAT4n_DJF(:,49) = FoamSAT4n_DJF(:,1);
FoamSATAIS1c_DJF(:,49) = FoamSATAIS1c_DJF(:,1);
FoamSATAIS2c_DJF(:,49) = FoamSATAIS2c_DJF(:,1);
FoamSATAIS3c_DJF(:,49) = FoamSATAIS3c_DJF(:,1);
FoamSAT2nc_DJF(:,49) = FoamSAT2nc_DJF(:,1);
FoamSAT3nc_DJF(:,49) = FoamSAT3nc_DJF(:,1);
FoamSAT4nc_DJF(:,49) = FoamSAT4nc_DJF(:,1);

% Topography is also loaded, in case it might be wanted
Foamtopo    = rot90(ncread('../FOAM/Topo.34Ma.WAM.nc', 'TOPO'));


%% Generate grids for interpolation
% Load lat and long data
Hadlon = double(ncread('../HadCM3BL/tecqno.pfclann.nc', 'longitude'));
Hadlat = double(ncread('../HadCM3BL/tecqno.pfclann.nc', 'latitude'));
Foamlon = double(ncread('../FOAM/FOAM.34Ma.WAM.2x.WSO.ISS1.atm.nc', 'lon'));
Foamlon(49) = 360;
Foamlat = double(ncread('../FOAM/FOAM.34Ma.WAM.2x.WSO.ISS1.atm.nc', 'lat'));
Foamtlat = double(ncread('../FOAM/Topo.34Ma.WAM.nc', 'MYLAT'));
Foamtlon = double(ncread('../FOAM/Topo.34Ma.WAM.nc', 'MYLON'));

% Generate grids to mesh between
[Had_resX, Had_resY] = meshgrid(Hadlon, Hadlat);
[Foam_resX, Foam_resY] = meshgrid(Foamlon, Foamlat);
[Foamt_resX, Foamt_resY] = meshgrid(Foamtlon, Foamtlat);


%% Interpolate SAT
FoamSAT_AIS1_i  = interp2(Foam_resX,Foam_resY,FoamSATAIS1,Had_resX,Had_resY);
FoamSAT_AIS2_i  = interp2(Foam_resX,Foam_resY,FoamSATAIS2,Had_resX,Had_resY);
FoamSAT_AIS3_i  = interp2(Foam_resX,Foam_resY,FoamSATAIS3,Had_resX,Had_resY);
FoamSAT2n_i  = interp2(Foam_resX,Foam_resY,FoamSAT2n,Had_resX,Had_resY);
FoamSAT3n_i  = interp2(Foam_resX,Foam_resY,FoamSAT3n,Had_resX,Had_resY);
FoamSAT4n_i  = interp2(Foam_resX,Foam_resY,FoamSAT4n,Had_resX,Had_resY);
FoamSAT_AIS1c_i  = interp2(Foam_resX,Foam_resY,FoamSATAIS1c,Had_resX,Had_resY);
FoamSAT_AIS2c_i  = interp2(Foam_resX,Foam_resY,FoamSATAIS2c,Had_resX,Had_resY);
FoamSAT_AIS3c_i  = interp2(Foam_resX,Foam_resY,FoamSATAIS3c,Had_resX,Had_resY);
FoamSAT2nc_i  = interp2(Foam_resX,Foam_resY,FoamSAT2nc,Had_resX,Had_resY);
FoamSAT3nc_i  = interp2(Foam_resX,Foam_resY,FoamSAT3nc,Had_resX,Had_resY);
FoamSAT4nc_i  = interp2(Foam_resX,Foam_resY,FoamSAT4nc,Had_resX,Had_resY);

FoamSAT_AIS1_DJF_i  = interp2(Foam_resX,Foam_resY,FoamSATAIS1_DJF,Had_resX,Had_resY);
FoamSAT_AIS2_DJF_i  = interp2(Foam_resX,Foam_resY,FoamSATAIS2_DJF,Had_resX,Had_resY);
FoamSAT_AIS3_DJF_i  = interp2(Foam_resX,Foam_resY,FoamSATAIS3_DJF,Had_resX,Had_resY);
FoamSAT2n_DJF_i  = interp2(Foam_resX,Foam_resY,FoamSAT2n_DJF,Had_resX,Had_resY);
FoamSAT3n_DJF_i  = interp2(Foam_resX,Foam_resY,FoamSAT3n_DJF,Had_resX,Had_resY);
FoamSAT4n_DJF_i  = interp2(Foam_resX,Foam_resY,FoamSAT4n_DJF,Had_resX,Had_resY);
FoamSAT_AIS1c_DJF_i  = interp2(Foam_resX,Foam_resY,FoamSATAIS1c_DJF,Had_resX,Had_resY);
FoamSAT_AIS2c_DJF_i  = interp2(Foam_resX,Foam_resY,FoamSATAIS2c_DJF,Had_resX,Had_resY);
FoamSAT_AIS3c_DJF_i  = interp2(Foam_resX,Foam_resY,FoamSATAIS3c_DJF,Had_resX,Had_resY);
FoamSAT2nc_DJF_i  = interp2(Foam_resX,Foam_resY,FoamSAT2nc_DJF,Had_resX,Had_resY);
FoamSAT3nc_DJF_i  = interp2(Foam_resX,Foam_resY,FoamSAT3nc_DJF,Had_resX,Had_resY);
FoamSAT4nc_DJF_i  = interp2(Foam_resX,Foam_resY,FoamSAT4nc_DJF,Had_resX,Had_resY);

Foamtopo_i  = interp2(Foamt_resX,Foamt_resY,Foamtopo,Had_resX,Had_resY);


%% Centre GM 
FoamSAT_AIS1_i = central_gm(FoamSAT_AIS1_i);
FoamSAT_AIS2_i = central_gm(FoamSAT_AIS2_i);
FoamSAT_AIS3_i = central_gm(FoamSAT_AIS3_i);
FoamSAT2n_i = central_gm(FoamSAT2n_i);
FoamSAT3n_i = central_gm(FoamSAT3n_i);
FoamSAT4n_i = central_gm(FoamSAT4n_i);
FoamSAT_AIS1c_i = central_gm(FoamSAT_AIS1c_i);
FoamSAT_AIS2c_i = central_gm(FoamSAT_AIS2c_i);
FoamSAT_AIS3c_i = central_gm(FoamSAT_AIS3c_i);
FoamSAT2nc_i = central_gm(FoamSAT2nc_i);
FoamSAT3nc_i = central_gm(FoamSAT3nc_i);
FoamSAT4nc_i = central_gm(FoamSAT4nc_i);


FoamSAT_AIS1_DJF_i = central_gm(FoamSAT_AIS1_DJF_i);
FoamSAT_AIS2_DJF_i = central_gm(FoamSAT_AIS2_DJF_i);
FoamSAT_AIS3_DJF_i = central_gm(FoamSAT_AIS3_DJF_i);
FoamSAT2n_DJF_i = central_gm(FoamSAT2n_DJF_i);
FoamSAT3n_DJF_i = central_gm(FoamSAT3n_DJF_i);
FoamSAT4n_DJF_i = central_gm(FoamSAT4n_DJF_i);
FoamSAT_AIS1c_DJF_i = central_gm(FoamSAT_AIS1c_DJF_i);
FoamSAT_AIS2c_DJF_i = central_gm(FoamSAT_AIS2c_DJF_i);
FoamSAT_AIS3c_DJF_i = central_gm(FoamSAT_AIS3c_DJF_i);
FoamSAT2nc_DJF_i = central_gm(FoamSAT2nc_DJF_i);
FoamSAT3nc_DJF_i = central_gm(FoamSAT3nc_DJF_i);
FoamSAT4nc_DJF_i = central_gm(FoamSAT4nc_DJF_i);

Foamtopo_i = central_gm(Foamtopo_i);

