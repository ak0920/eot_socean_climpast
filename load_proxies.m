% load_proxies.m
% 
% Read in the temperature proxy data from the CSV files and do some basic
% processing
% 

%% Read in tables
EoceneTemp = readtable('../Proxies/CP_Eocene.csv');
EOTTemp = readtable('../Proxies/CP_EOT.csv');
OligoceneTemp = readtable('../Proxies/CP_Oligocene.csv');


%% Change -999s to NaN
for i = 1:height(EoceneTemp)
    for j = 1:width(EoceneTemp)
        temp = EoceneTemp{i,j};
        if isnumeric(temp)
            if temp == -999
                EoceneTemp{i,j} = nan;
            end
        end
    end
end

for i = 1:height(EOTTemp)
    for j = 1:width(EOTTemp)
        temp = EOTTemp{i,j};
        if isnumeric(temp)
            if temp == -999
                EOTTemp{i,j} = nan;
            end
        end
    end
end

for i = 1:height(OligoceneTemp)
    for j = 1:width(OligoceneTemp)
        temp = OligoceneTemp{i,j};
        if isnumeric(temp)
            if temp == -999
                OligoceneTemp{i,j} = nan;
            end
        end
    end
end

