% load_Temp_response
% 
% Finds the difference between all model siumaltions that make up
% 'EOT-like' pairs, i.e. include ice growth and potentially another
% forcing, saved in a structure called TempEOTdjf. This is done manually
% because it is easier.
% 


%% Load the simulation data
load_HadCM3BL
load_FOAM


%% Find the differences

% Ice expansion only
TempEOT.end3ficeO = Temp.end3fO - Temp.end3nO;
TempEOT.end2ficeO = Temp.end2fO - Temp.end2nO;
TempEOT.end3ficeC = Temp.end3fC - Temp.end3nC;
TempEOT.end2ficeC = Temp.end2fC - Temp.end2nC;

TempEOT.Fsice2 = FoamSAT_AIS1_i - FoamSAT2n_i;
TempEOT.Feice2 = FoamSAT_AIS2_i - FoamSAT2n_i;
TempEOT.Ffice2 = FoamSAT_AIS3_i - FoamSAT2n_i;
TempEOT.Fsice2c = FoamSAT_AIS1c_i - FoamSAT2nc_i;
TempEOT.Feice2c = FoamSAT_AIS2c_i - FoamSAT2nc_i;
TempEOT.Ffice2c = FoamSAT_AIS3c_i - FoamSAT2nc_i;


% pCO2 (+ ice)
TempEOT.endfCO2O = Temp.end2fO - Temp.end3nO;
TempEOT.endfCO2C = Temp.end2fC - Temp.end3nC;

TempEOT.FsCO2 = FoamSAT_AIS1_i - FoamSAT3n_i;
TempEOT.FeCO2 = FoamSAT_AIS2_i - FoamSAT3n_i;
TempEOT.FfCO2 = FoamSAT_AIS3_i - FoamSAT3n_i;
TempEOT.FsCO22 = FoamSAT_AIS1_i - FoamSAT4n_i;
TempEOT.FeCO22 = FoamSAT_AIS2_i - FoamSAT4n_i;
TempEOT.FfCO22 = FoamSAT_AIS3_i - FoamSAT4n_i;
TempEOT.FsCO2c = FoamSAT_AIS1c_i - FoamSAT3nc_i;
TempEOT.FeCO2c = FoamSAT_AIS2c_i - FoamSAT3nc_i;
TempEOT.FfCO2c = FoamSAT_AIS3c_i - FoamSAT3nc_i;
TempEOT.FsCO22c = FoamSAT_AIS1c_i - FoamSAT4nc_i;
TempEOT.FeCO22c = FoamSAT_AIS2c_i - FoamSAT4nc_i;
TempEOT.FfCO22c = FoamSAT_AIS3c_i - FoamSAT4nc_i;


% Palaeogeog. (+ ice)
TempEOT.end3fgeog = Temp.end3fO - Temp.end3nC;
TempEOT.end2fgeog = Temp.end2fO - Temp.end2nC;


% Palaeogeog. + pCO2 (+ ice)
TempEOT.endall = Temp.end2fO - Temp.end3nC;


