% proxy_data_subplot.m
% 
% This script subplots the proxy records around the map for Figure 1 of 
% Kennedy-Aser et al. 2020 (Clim. Past). site_name fust be dfined as a 
% string before running it, e.g. 'MaudRise', as it is defined in the CSV file.
% 
% Also remember to change the subplot location and give the subplot a title.
% 

hold on

% Take Eocene data from table
xe = sum(strcmp(EoceneTemp{:,1},site_name)); % number of records
ymeane = EoceneTemp{strcmp(EoceneTemp{:,1},site_name),4};
ymaxe = EoceneTemp{strcmp(EoceneTemp{:,1},site_name),5};
ymine = EoceneTemp{strcmp(EoceneTemp{:,1},site_name),6};
yproxe = EoceneTemp{strcmp(EoceneTemp{:,1},site_name),8};

% Take Oligocene data from table
xo = sum(strcmp(OligoceneTemp{:,1},site_name)); % number of records
ymeano = OligoceneTemp{strcmp(OligoceneTemp{:,1},site_name),4};
ymaxo = OligoceneTemp{strcmp(OligoceneTemp{:,1},site_name),5};
ymino = OligoceneTemp{strcmp(OligoceneTemp{:,1},site_name),6};
yproxo = OligoceneTemp{strcmp(OligoceneTemp{:,1},site_name),8};

% Combine for plotting axis labels
xx = 1:(xe+xo); % combined number of records 
yprox = [yproxe; yproxo];


% Plot Eocene data
for i = 1:xe
    if isnan(ymine(i))
        plot(i,ymaxe(i),'v','color',red,'markersize',10,'markerfacecolor',red)
        
    else
        if isnan(ymaxe(i))
            plot(i,ymine(i),'^','color',red,'markersize',10,'markerfacecolor',red)
        else
            plot([i i],[ymaxe(i) ymine(i)],'-','color',red,'linewidth',3)
            plot(i,ymaxe(i),'v','color',red,'markersize',10,'markerfacecolor',red)
            plot(i,ymine(i),'^','color',red,'markersize',10,'markerfacecolor',red)
            plot(i,ymeane(i),'.','color',red,'markersize',30)
        end
    end
end

% Plot Oligocene data
for i = (xe+1):(xe+xo)
    ii = i - xe;
    if isnan(ymino(ii))
        plot(i,ymaxo(ii),'v','color',blue,'markersize',10,'markerfacecolor',blue)
    else
        if isnan(ymaxo(ii))
            plot(i,ymino(ii),'^','color',blue,'markersize',10,'markerfacecolor',blue)
        else
            plot([i i],[ymaxo(ii) ymino(ii)],'-','color',blue,'linewidth',3)
            plot(i,ymaxo(ii),'v','color',blue,'markersize',10,'markerfacecolor',blue)
            plot(i,ymino(ii),'^','color',blue,'markersize',10,'markerfacecolor',blue)
            plot(i,ymeano(ii),'.','color',blue,'markersize',30)
        end
    end
end


% Plot line between Eocene and Oligocene data
ylims = ylim;
plot([xe+0.5 xe+0.5],[min(ylims) max(ylims)],':k')

% Tidy figure
ax = gca;
ax.YGrid = 'on';
xlim([0 xe+xo+1])
set(gca, 'XTick', xx, 'XTickLabel',{yprox{:}}, 'fontname',font,'fontsize',10)
xtickangle(45)
ylabel('SAT (�C)')
box on
