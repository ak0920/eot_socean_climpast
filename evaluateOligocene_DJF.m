% evaluateOligocene_DJF.m
% 
% Carry out the evaluation of DJF from each simulation compared to Oligocene data and
% plot Supp. Figure 2b. Resizing and merging with Supp. Figure 2a was carried out
% separately/manually.
% 

clear

%% Load the data
% HadCM3BL
load_HadCM3BL

% FOAM
load_FOAM

% Load other models into Temp structure manually:
Tempdjf.FoamAIS1 = FoamSAT_AIS1_DJF_i;
Tempdjf.FoamAIS2 = FoamSAT_AIS2_DJF_i;
Tempdjf.FoamAIS3 = FoamSAT_AIS3_DJF_i;
Tempdjf.FoamAIS1c = FoamSAT_AIS1c_DJF_i;
Tempdjf.FoamAIS2c = FoamSAT_AIS2c_DJF_i;
Tempdjf.FoamAIS3c = FoamSAT_AIS3c_DJF_i;

% Proxy data
load_proxies

% Load latitudinal gradient data
load_latgrads

% Create benchmark data
Tempdjf.bm_const = nanmean(OligoceneTemp{:,4})*ones(73,96);
Tempdjf.bm_latgr = repmat(flipud(lats)*(Oligocenem)+Oligocenec,1,96); 


%% Calculate RMSEs
desc = {'end3fC','end3fO','end2fC','end2fO','FoamAIS1','FoamAIS2','FoamAIS3','FoamAIS1c','FoamAIS2c','FoamAIS3c','bm_const','bm_latgr'};
[OligoceneRMSE, OligoceneRMSEnorm, OligoceneCount, OligoceneModelMeans, OligoceneEs, OligocenenormEs] = evaluate_RMSE(OligoceneTemp,Tempdjf,desc);


%% Convert structures to arrays for plotting

for j = 1:length(desc)
    
    % Convert cells (simulation descriptions) into useable string
    desc_cell = desc(j);
    desc_n = sprintf('%s',desc_cell{:});
    
    
    % Calculate RMSE (note: could probably apply an area weighting here,
    % but I have not)
    RMSEplot(j) = OligoceneRMSE.(desc_n);

    RMSEnormplot(j) = OligoceneRMSEnorm.(desc_n);
    
    CountPlot(j) = OligoceneCount.(desc_n);
    
end


%% Prepare to plot
load('warm_cols.mat')
font = 'Arial';
fontsz = 14;
clim = [0 8];

% Find how many records there are (to normalise the count metric /1)
nodata=sum(strcmp(OligoceneTemp{:,8},'nodata'))+sum((strcmp(OligoceneTemp{:,8},'Dinocysts') + ~isnan(OligoceneTemp{:,6}))>1);
CountNorm = max(clim)*(height(OligoceneTemp)-nodata-CountPlot)/(height(OligoceneTemp)-nodata);

% Put variables to plot into an array
plotting = [RMSEplot;RMSEnormplot;CountNorm];

ydesc = {'Standard RMSE (�C)', 'Normalised RMSE (�C)', ['Count (within data error bars /',num2str(height(OligoceneTemp)-nodata),')']};
xdesc = {'3x pCO_2, closed DP, EAIS','3x pCO_2, open DP, EAIS','2x pCO_2, closed DP, EAIS','2x pCO_2, open DP, EAIS',...
    '2x pCO_2, warm orbit, small AIS','2x pCO_2, warm orbit, EAIS','2x pCO_2, warm orbit, full AIS', ...
    '2x pCO_2, cold orbit, small AIS','2x pCO_2, cold orbit, EAIS','2x pCO_2, cold orbit, full AIS', ...
    'Constant mean','Latitudinal gradient'};


%% Plot
imagesc(plotting)
colormap(warm_cols)
caxis(clim)

% Add lines between model groups
hold on
plot([4.5 4.5],[0.5 3.5],'-k','linewidth',2)
plot([10.5 10.5],[0.5 3.5],'-k','linewidth',2)
plot([0.5 25.5],[1.5 1.5],'-k')
plot([0.5 25.5],[2.5 2.5],'-k')

% Add values for each metric
for i = 1:length(xdesc)
    text(i,3,sprintf('%.0f',CountPlot(1,i)),'HorizontalAlignment','center', 'fontname', font, 'fontsize', fontsz)
    text(i,2,sprintf('%.1f',RMSEnormplot(1,i)),'HorizontalAlignment','center', 'fontname', font, 'fontsize', fontsz)
    text(i,1,sprintf('%.1f',RMSEplot(1,i)),'HorizontalAlignment','center', 'fontname', font, 'fontsize', fontsz)
end

% Tidy up
set(gca, 'XTick',1:length(xdesc), 'XTickLabel',xdesc, 'YTick',1:length(ydesc), 'YTickLabel',ydesc, 'fontname', font, 'fontsize', fontsz)
xtickangle(35)
axis equal
xlim([0.5 0.5+length(xdesc)])
ylim([0.5 3.5])

text(2.5,0,'HadCM3BL','HorizontalAlignment','center','fontweight','bold', 'fontname', font, 'fontsize', fontsz)
text(7.5,0,'FOAM','HorizontalAlignment','center','fontweight','bold', 'fontname', font, 'fontsize', fontsz)
text(11.5,0,'Benchmarks','HorizontalAlignment','center','fontweight','bold', 'fontname', font, 'fontsize', fontsz)
text(-4.5,0,'b','HorizontalAlignment','center','fontweight','bold', 'fontname', font, 'fontsize', 16)


%% Add indicators for moderate or good performance
% These are added manually

% Moderate performance (1 star)
xmod = [2 4 5 6 7 8 9 10 ...
    2 4 5 6 8 9 10 ...
    ];
ymod = [1 1 1 1 1 1 1 1 ...
    2 2 2 2 2 2 2 ...
    ];
plot(xmod,ymod+0.35,'pk')

% Good performance (2 stars)
xgood = [7 ];
ygood = [2 ];
plot(xgood-0.15,ygood+0.35,'pk', 'markerfacecolor','k')
plot(xgood+0.15,ygood+0.35,'pk', 'markerfacecolor','k')


%% Tidy up
c = colorbar;
ylabel(c, '(Normalised) RMSE (�C)', 'fontname', font, 'fontsize', fontsz)
set(c, 'fontname', font, 'fontsize', fontsz) 

set(gcf, 'color', 'w');
% set(gcf,'position',[0 0 800 300],'units','normalized')
