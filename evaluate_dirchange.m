function [CountDir] = evaluate_dirchange(ProxyData, ModelData, Desc)
% [CountDir] = evaluate_dirchange(ProxyData, ModelData, Desc)
% 
% For EOT data, calculate if model simulation pairings correct the correct
% diraction of temperature change across the EOT.
% 
% Outputs:
%   CountDir = No. of sites/records at which direction of change is 
%       correctly captured
% 
% Inputs:
%   ProxyData = The proxy data for the EOT (i.e. CP_EOT.csv)
%   ModelData = strucutre containing the loaded model data for the EOT
%   Desc = field names of individual simulations in the ModelData structure
% 

%% Generate area grids
proc_latlon_area % This loads areas_abs
areas_abs = rot90(areas_abs);

% Model lat-lon data
latsmodel = fliplr(-90:2.5:90);
lonsmodel = -180:3.75:176.25;


%% Find SAT/SST at each location
% Loop through each site, then through each model

% Create an empty array to save mean values in
ModelMeans = nan(length(Desc),height(ProxyData));
CountDir = zeros(1,length(Desc));

% Site names, taken from CSV table
for i = 1:height(ProxyData)
    
    % Find the model location of each site
    [~,sitelat] = min(abs(latsmodel-ProxyData{i,2}));
    [~,sitelon] = min(abs(lonsmodel-ProxyData{i,3}));
    
    % Include � 1 grid cell in lat and long
    sitelats = [sitelat-1, sitelat, sitelat+1];
    
    if sitelon == 1 
        sitelons = [96, 1, 2];
    else
        if sitelon == 96
            sitelons = [95, 96, 1];
        else
            sitelons = [sitelon-1, sitelon, sitelon+1];
        end
    end
    
    
    % Go through each model simulation
    for j = 1:length(Desc)
        
        % Convert cells (2x CO2 identifiers) into useable string
        desc_cell = Desc(j);
        desc_n = sprintf('%s',desc_cell{:});
        
        % Read the model simulation data into a temporary file
        temp = ModelData.(desc_n);
        
        % Mean value from model around site location (store as an array to output/plot later)
        ModelMeans(j,i) = sum(sum(temp(sitelats,sitelons).* areas_abs(sitelats,sitelons)))/sum(sum(areas_abs(sitelats,sitelons)));
        
        if ProxyData{i,7} == -1
            if ModelMeans(j,i) < 0
                CountDir(j) = CountDir(j) + 1;
            end
        else
            if ProxyData{i,7} == 1
                if ModelMeans(j,i) >0
                    CountDir(j) = CountDir(j) + 1;
                end
            end
        end

        
    end
end


