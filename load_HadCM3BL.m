% load_HadCM3BL.m
% 
% This script loads all of the HadCM3BL simulations used in Kennedy-Asser
% et al. 2020 (Clim. Past). Simulations carried out by P.J. Valdes and are
% those used in Kennedy-Asser et al. 2019 (Paleoceanography and
% Paleoclimatology).
% 
% The format of the data is:
%    VARIABLE.end[CO2][ice sheet][DRAKE PASSAGE]
% where:
%    CO2 = 3 or 2 (x pre-industrial levels)
%    ice sheet = n (no ice), e (EAIS) or f (full AIS)
%    DRAKE PASSAGE = O (DP = open), C (DP = closed) 


%% Load lat-long. info (can be useful later on)
lats = ncread('../HadCM3BL/tecqno.pfclann.nc','latitude');
lons = ncread('../HadCM3BL/tecqno.pfclann.nc','longitude');


%% List of simulation names
models = {'tecqn1','tecqo2','tecqq1','tecqp2','tecqs1','tecqt3','tecqv1','tecqu2'};

% More useful descriptions of the simulations
desc = {'end2fO','end2fC','end2nO','end2nC','end3fO','end3fC','end3nO','end3nC'};


%% Load the data
for i = 1:length(models)
    
    % Convert cells into useable string
    desc_cell = desc(i);
    desc_str = sprintf('%s',desc_cell{:});
    model_cell = models(i);
    model_str = sprintf('%s',model_cell{:}); % this can be used to find the model folder on BRIDGE machines, including the simulation cont. number, e.g. tecqt3
    model_str2 = model_str(1:5); % this is used for the .nc files which do not include the simulation cont. number e.g. tecqt
    
    datadir = '../HadCM3BL/'; % HadCM3BL runs should be stored here
    
    % Make string names for the various netCDFs
    netcdf1 = [datadir,model_str2,'o.pfclann.nc'];
    netcdf1a = [datadir,model_str2,'o.pfcldjf.nc'];
    netcdf2 = [datadir,model_str2,'a.pdclann.nc'];
    netcdf2a = [datadir,model_str2,'a.pdcldjf.nc'];
    
    % Load SST and add to structure
    SST.(desc_str) = rot90(central_gm(ncread(netcdf1, 'temp_mm_uo'))); % units = �C
    SSTdjf.(desc_str) = rot90(central_gm(ncread(netcdf1a, 'temp_mm_uo'))); % units = �C
    
    % Load SAT and add to structure
    SAT.(desc_str) = rot90(central_gm(fliplr(ncread(netcdf2, 'temp_mm_1_5m'))-273.15)); % convert K -> �C
    
    SATdjf.(desc_str) = rot90(central_gm(fliplr(ncread(netcdf2a, 'temp_mm_1_5m'))-273.15)); % convert K -> �C
    
    % Generate LSM in case that might be useful
    LSM.(desc_str) = ~isnan(rot90(central_gm(ncread(netcdf1,'temp_mm_uo'))));
    
    % Create a combined SST/SAT field
    Temp.(desc_str) = SST.(desc_str);
    Temp.(desc_str)(isnan(Temp.(desc_str))) = SAT.(desc_str)(isnan(Temp.(desc_str)));
    
    Tempdjf.(desc_str) = SSTdjf.(desc_str);
    Tempdjf.(desc_str)(isnan(Tempdjf.(desc_str))) = SATdjf.(desc_str)(isnan(Tempdjf.(desc_str)));
    
end
