% plot_latgrads_comparison_err.m
%
% Plot the proxy data for each time slice against latitude in comparison
% with the best HadCM3BL and FOAM simulations. This loads the model error
% shading only: Matlab on my Mac could only export this in an 'opaque' way
% when lines were also included, so the error shading is saved separately
% and stitched manually in image editing software.
% 
% For creation of Figure 3 in Kennedy-Asser et al. 2020 (Clim. Past)
%


%% Load data
load_proxies
load_HadCM3BL
load_FOAM
load_Temp_response

% Load colours and set some image defaults
red = [0.850000000000000,0.0872549042105675,0];
orange = [1,0.400000005960465,0.200000002980232];
green = [0.530028571428571,0.749114285714286,0.466114285714286];
blue = [0.0585574759170413,0.364924505352974,0.679114878177643];
grey = [0.6 0.6 0.6];
lgrey = [0.8 0.8 0.8];
font = 'Arial';
fontsz = 11;


%% Plot HadCM3BL error first
figure

% Repeat the same procedure for each dataset/time slice
for i = 1:3
    % Select data for each subplot
    if i ==1
        Data = EoceneTemp;
    else
        if i ==2
            Data = OligoceneTemp;
        else
            Data = EOTTemp;
        end
    end
    
    
    % Generate ranges for plotting error bars
    rangelats = nan;
    ranges = nan;
    
    for j = 1:height(Data)
        if ~isnan(Data{j,5}) && ~isnan(Data{j,6})
            
            rangelats(length(rangelats)+1) = Data{j,2};
            rangelats(length(rangelats)+1) = Data{j,2};
            rangelats(length(rangelats)+1) = nan;
            
            ranges(length(ranges)+1) = Data{j,5};
            ranges(length(ranges)+1) = Data{j,6};
            ranges(length(ranges)+1) = nan;
        end
    end
    
    
    %% Plot figures
    subplot(1,3,i)
    hold on
    if i == 1
        err = fill([Hadlat(5:17); flipud(Hadlat(5:17))],[flipud(max(FoamSAT4nc_i(57:69,:),[],2)); min(FoamSAT4nc_i(57:69,:),[],2)],red,'LineStyle','none');
        alpha(err,0.2)
        
        ylim([-20 30])

    else
        if i ==2
            err = fill([Hadlat(5:17); flipud(Hadlat(5:17))],[flipud(max(FoamSAT_AIS1c_i(57:69,:),[],2)); min(FoamSAT_AIS1c_i(57:69,:),[],2)],red,'LineStyle','none');
            alpha(err,0.2)
            ylim([-20 30])

        else
            err = fill([Hadlat(5:17); flipud(Hadlat(5:17))],[flipud(max(TempEOT.FsCO22c(57:69,:),[],2)); min(TempEOT.FsCO22c(57:69,:),[],2)],red,'LineStyle','none');
            alpha(err,0.2)


            ylim([-30 5])
        end
    end

    % Tidy up
    box off
    axis off
    set(gca, 'fontname',font,'fontsize',fontsz)


end

% Tidy up
set(gcf, 'color', 'w');
set(gcf,'position',[0 0 1200 350],'units','normalized')


%% Plot FOAM error second
figure


% Repeat the same procedure for each dataset/time slice
for i = 1:3
    % Select data for each subplot
    if i ==1
        Data = EoceneTemp;
    else
        if i ==2
            Data = OligoceneTemp;
        else
            Data = EOTTemp;
        end
    end
    
    
    % Generate ranges for plotting error bars
    rangelats = nan;
    ranges = nan;
    
    for j = 1:height(Data)
        if ~isnan(Data{j,5}) && ~isnan(Data{j,6})
            
            rangelats(length(rangelats)+1) = Data{j,2};
            rangelats(length(rangelats)+1) = Data{j,2};
            rangelats(length(rangelats)+1) = nan;
            
            ranges(length(ranges)+1) = Data{j,5};
            ranges(length(ranges)+1) = Data{j,6};
            ranges(length(ranges)+1) = nan;
        end
    end
    
    
    %% Plot figures
    subplot(1,3,i)
    hold on
    if i == 1
        
        err = fill([Hadlat(5:17); flipud(Hadlat(5:17))],[flipud(max(Temp.end3nO(57:69,:),[],2)); min(Temp.end3nO(57:69,:),[],2)],blue,'LineStyle','none');
        alpha(err,0.2)
        ylim([-20 30])

    else
        if i ==2
            err = fill([Hadlat(5:17); flipud(Hadlat(5:17))],[flipud(max(Temp.end3fO(57:69,:),[],2)); min(Temp.end3fO(57:69,:),[],2)],blue,'LineStyle','none');
            alpha(err,0.2)
            ylim([-20 30])

        else
            err = fill([Hadlat(5:17); flipud(Hadlat(5:17))],[flipud(max(TempEOT.end3ficeO(57:69,:),[],2)); min(TempEOT.end3ficeO(57:69,:),[],2)],blue,'LineStyle','none');
            alpha(err,0.2)


            ylim([-30 5])
        end
    end

    % Tidy up
    box off
    axis off
    set(gca, 'fontname',font,'fontsize',fontsz)
    
end

% Tidy up
set(gcf, 'color', 'w');
set(gcf,'position',[0 0 1200 350],'units','normalized')
